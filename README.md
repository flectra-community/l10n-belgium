# Flectra Community / l10n-belgium

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[companyweb_base](companyweb_base/) | 2.0.1.1.1| Know who you are dealing with. Enhance Odoo partner data from companyweb.be.
[l10n_be_partner_kbo_bce](l10n_be_partner_kbo_bce/) | 2.0.1.0.1| Belgium - KBO/BCE numbers
[companyweb_payment_info](companyweb_payment_info/) | 2.0.1.1.0| Send your customer bill to companyweb
[account_statement_import_coda](account_statement_import_coda/) | 2.0.1.0.2| Import CODA Bank Statement
[l10n_be_mis_reports](l10n_be_mis_reports/) | 2.0.1.0.1|         MIS Builder templates for the Belgium P&L,        Balance Sheets and VAT Declaration


